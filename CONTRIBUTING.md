Contributions very welcome!

## Making database changes

The database is just a cache, we store no 'original' information. For that
reason, it's fine to make changes to the database layout without making neat
migrations. When you have pulled a branch with a different database layout,
just drop your database file and repopulate it with the new layout. Perhaps
set it aside as a backup when you switch branches a lot.

To change the database layout:
- run `diesel migration revert`
- make any desired changes to `migrations/main/up.sql` (and, if needed, to `down.sql`)
- run `diesel migration run` (this step will also update `src/schema.rs`)
- run `diesel migration redo`
- run `cargo build` and fix all compiler errors (you'll likely want to update `src/models.rs` and take it from there)
