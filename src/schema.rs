table! {
    items (id) {
        id -> Integer,
        kind -> Text,
        draft -> Integer,
        number -> Integer,
        package -> Text,
        title -> Text,
        html_url -> Text,
    }
}
