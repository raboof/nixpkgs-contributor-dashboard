extern crate nixpkgs_contributor_dashboard;
extern crate diesel;
extern crate dotenv;

use std::convert::TryInto;
use std::env;

use self::nixpkgs_contributor_dashboard::*;
use self::models::*;
use self::schema::*;
use self::diesel::prelude::*;
use serde::Deserialize;
use regex::Regex;
use dotenv::dotenv;
use octocrab::Octocrab;
use octocrab::models::pulls::PullRequest;

#[derive(Deserialize, Debug)]
pub struct Pull {
    pub id: i32,
    pub number: i32,
    pub draft: bool,
    pub title: String,
    pub html_url: String,
}

fn process_pulls(connection: &SqliteConnection, the_pulls: &Vec<PullRequest>) {
    for pull in the_pulls {
        let package: String = pull.title.clone().unwrap().as_str().chars().take_while(|c| *c != ':').collect();
        let title = pull.title.clone().unwrap();
        let html_url = pull.html_url.clone().unwrap();
        let new_item = NewItem {
            kind: "PR",
            draft: if pull.draft.unwrap() { 1 } else { 0 },
            number: pull.number.try_into().unwrap(),
            package: package.as_str(),
            title: title.as_str(),
            html_url: html_url.as_str()
        };
        println!("{}", pull.number);
        diesel::insert_into(items::table)
            .values(&new_item)
            // Why does 'on_conflict()' not work here?
            .execute(connection)
            .expect("Error saving pull");
    }
}

async fn fetch_page(client: &Octocrab, connection: &SqliteConnection, page: u32) {
    let page = client
        .pulls("nixos", "nixpkgs")
        .list()
        .per_page(100)
        .page(page)
        .send()
        .await;
    process_pulls(&connection, &page.unwrap().take_items());
}

#[tokio::main]
async fn main() -> octocrab::Result<()> {
    dotenv().ok();
    let client =
      Octocrab::builder().personal_token(env::var("GITHUB_TOKEN").unwrap()).build()?;
    let mut page = client
        .pulls("nixos", "nixpkgs")
        .list()
        .per_page(100)
        .send()
        .await?;
    let connection = establish_connection();

    process_pulls(&connection, &page.take_items());
    for i in 2..(page.number_of_pages().unwrap() - 1) {
        fetch_page(&client, &connection, i);
    }
   Ok(())
}
