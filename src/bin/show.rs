extern crate nixpkgs_contributor_dashboard;
extern crate diesel;

use self::nixpkgs_contributor_dashboard::*;
use self::models::*;
use self::diesel::prelude::*;

use serde::Deserialize;

use std::env;
use std::fs;
use std::collections::HashMap;
use std::path::Path;

use regex::Regex;
use regex::escape;

#[derive(Deserialize)]
pub struct Config {
  pub interests: Vec<String>,
}

pub fn parse_config(path: &str) -> Config {
    if Path::new(path).exists() {
        // TODO not sure it's even necessary to explicitly go via string here:
        let config_string: std::string::String = fs::read_to_string(path).unwrap();
        toml::from_str(config_string.as_str()).unwrap()
    } else {
        Config {
            interests: Vec::new(),
        }
    }
}

// returns 'None' if the item doesn't match any of the keys we're interested
// in, or the key we're interested in.
fn interesting<'a>(item: &Item, interests: &'a Vec<String>, map: &HashMap<&str, Regex>) -> Option<&'a str> {
    if item.draft == 1 {
        return None
    }

    if interests.is_empty() {
        return Some("")
    }

    let pkg: &str = item.package.as_str();

    //println!("Considering {}", pkg);
    for interest in interests {
        if map.get(interest.as_str()).unwrap().is_match(pkg) {
            return Some(interest)
        }
    }

    return None
}

fn print(item: Item, interest: &str) {
    println!("{} #{}:\t[{}/{}]\t{}\t{}", item.kind, item.number, interest, item.package, item.title, item.html_url);
}

fn create_regex_map(interests: &Vec<String>) -> HashMap<&str, Regex> {
    let mut result: HashMap<&str, Regex> = HashMap::new();
    for interest in interests {
        result.insert(
            interest.as_str(),
            Regex::new(format!("\\b{}\\b", escape(interest.as_str())).as_str()).unwrap()
        );
    }
    result
}

fn main() {
    use nixpkgs_contributor_dashboard::schema::items::dsl::*;

    let args: Vec<String> = env::args().collect();
    let config =
      if args.len() > 1 {
          parse_config(&args[1])
      } else {
          parse_config("config.toml")
      };
    let regexes = create_regex_map(&config.interests);

    let connection = establish_connection();
    let results = items.load::<Item>(&connection)
        .expect("Error loading items");
    println!("Database contains {} items", results.len());
    for item in results {
        if let Some(interest) = interesting(&item, &config.interests, &regexes) {
            print(item, interest)
        }
    }
}
