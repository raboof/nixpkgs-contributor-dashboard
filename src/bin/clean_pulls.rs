extern crate nixpkgs_contributor_dashboard;
extern crate diesel;
extern crate dotenv;

use std::convert::TryInto;
use std::env;

use self::nixpkgs_contributor_dashboard::*;
use self::models::*;
use self::diesel::prelude::*;

use serde::Deserialize;
use dotenv::dotenv;

use octocrab::Octocrab;
use octocrab::models::IssueState::Closed;
use octocrab::Error;

use nixpkgs_contributor_dashboard::schema::items::dsl::*;

#[derive(Deserialize, Debug)]
pub struct Pull {
    pub state: String,
}

async fn closed(client: &Octocrab, item: &Item) -> bool {
    let owner = "nixos";
    let repo = "nixpkgs";
    println!("Getting pr {}", item.html_url);
    let pr = client
        .pulls(owner, repo)
        .get(item.number.try_into().unwrap())
        .await;
    match pr {
        Ok(pr) =>
            return pr.state == Some(Closed),
        Err(Error::GitHub{source, ..}) => {
            match source.message.as_str() {
                "Not Found" => return true,
                other => {
                    println!("GitHub: {}", other);
                    return false
                }
            }
        },
        Err(e) => {
            println!("{}", e);
            return false
        }
    }
}

#[tokio::main]
async fn main() -> octocrab::Result<()> {
    dotenv().ok();
    let client =
      Octocrab::builder().personal_token(env::var("GITHUB_TOKEN").unwrap()).build()?;

    let connection = establish_connection();
    let results = items.filter(kind.eq("PR")).load::<Item>(&connection)
        .expect("Error loading items");
    for item in results {
        if closed(&client, &item).await {
            println!("Deleting {}", item.html_url);
            diesel::delete(&item).execute(&connection).expect("Error deleting");
        } else {
            println!("Keeping {}", item.html_url);
        }
    }
    Ok(())
}
