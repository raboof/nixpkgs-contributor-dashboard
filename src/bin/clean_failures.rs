extern crate nixpkgs_contributor_dashboard;
extern crate diesel;
extern crate dotenv;

use self::nixpkgs_contributor_dashboard::*;
use self::models::*;
use self::diesel::prelude::*;

use dotenv::dotenv;

use nixpkgs_contributor_dashboard::schema::items::dsl::*;

fn main() {
    dotenv().ok();

    let connection = establish_connection();
    let results = items.filter(kind.eq("BUILD")).load::<Item>(&connection)
        .expect("Error loading items");
    for item in results {
        diesel::delete(&item).execute(&connection).expect("Error deleting");
    }
}
