use super::schema::items;

#[derive(Queryable, Identifiable)]
pub struct Item {
    pub id: i32,
    // 'PR', 'ISSUE' or 'BUILD'
    pub kind: String,
    // 1 if draft PR, 0 otherwise
    pub draft: i32,
    pub number: i32,
    pub package: String,
    pub title: String,
    pub html_url: String,
}

#[derive(Insertable)]
#[table_name="items"]
pub struct NewItem<'a> {
    // 'PR' or 'BUILD'
    pub kind: &'a str,
    // For PR's, 1 if draft, 0 otherwise.
    pub draft: i32,
    pub number: i32,
    pub package: &'a str,
    pub title: &'a str,
    pub html_url: &'a str,
}


