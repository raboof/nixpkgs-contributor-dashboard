{ pkgs ? import <nixpkgs> {} }:

pkgs.mkShell {
  buildInputs = [
    pkgs.cargo
    pkgs.cargo-license
    pkgs.rustc
    pkgs.pkg-config
    pkgs.openssl
    pkgs.jq
    pkgs.diesel-cli
    pkgs.sqlite
  ];
}
