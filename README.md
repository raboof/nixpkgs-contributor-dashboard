# Nix contributor dashboard

The goal of this project is to collect and present information that is
relevant for nixpkgs contributors.

Specifically, I hope it will make it easier for 'casual' contributors to
keep track of packages that interest them and help out in maintenance tasks
when they have some time to spare.

If this is interesting to you https://github.com/jonringer/basinix might
also be worth checking out.

## General approach

This project will consist of a couple of processes that fetch information
from different sources (github issues, github PR's, build failures) and stick
it into a database - focusing on currently-actionable items (so we can drop
closed issues/prs etc)

Right now you can query this database with `bin/show` passing it a `config.yml`
containing a list of packages you are interested in (because you maintain them 
or use them), like this:

```
interests = [
  "inkcut",
  "notion",
  "protoc-gen-go-grpc",
]
```

It will then show open issues, PR's and current build failures for those packages.

This database is intended to contain only 'derived' information, so it can
always be thrown away and repopulated.

## Usage

Create a '.env' with 2 environment variables:
* GITHUB\_TOKEN (to get better GitHub API limits)
* DATABASE\_URL=nixpkgs\_dashboard\_data.sql

Add PR's to the db:

    cargo run --bin fetch_pulls

Add build failures to the db:

    cargo run --bin fetch_failures

Show items:

    cargo run --bin show

Clean up closed PR's:

    cargo run --bin clean_pulls

Clean up failures:

    cargo run --bin clean_failures && cargo run --bin fetch_failures

Clear the database:

    diesel migration redo

### Configuration

You can create a `config.toml` to list which packages you are interested in:

```
interests = [
  "diffoscope",
  "notion",
]
```

## Random notes

### Database

Let's use a sqlite db for now.

### Fetching from github

https://github.com/github-rs/github-rs can perform github API requests, but
currently you'd need to do your own pagination (https://github.com/github-rs/github-rs/issues/18)
Perhaps https://github.com/github-rs/github-rs/tree/master/github-gql-rs would
be more convenient.
